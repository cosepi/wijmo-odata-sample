﻿/// <binding BeforeBuild='Run - Development' />
"use strict";

module.exports = {
    mode: 'development',
    entry: {
        home: './Scripts/site/Home/Index.js'
    },
    output: {
        path: __dirname + "/Scripts/app",
        filename: "[name].bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ['style-loader','css-loader','sass-loader']
            }
        ]
    }
};