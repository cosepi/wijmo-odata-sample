﻿import '@grapecity/wijmo.styles/wijmo.scss';
import '@grapecity/wijmo.cultures/wijmo.culture.ja';
import { format } from '@grapecity/wijmo';
import { ODataCollectionView, ODataVirtualCollectionView } from '@grapecity/wijmo.odata';
import { FlexGrid, CellType } from '@grapecity/wijmo.grid';
import { FlexGridFilter } from '@grapecity/wijmo.grid.filter';

$(function () {
    var url = 'https://localhost:44384/odata/';
    var itemCountElement = document.getElementById('itemCount');
    var products = new ODataVirtualCollectionView(url, 'Products', {
        keys: ['Id'],
        loaded: function (sender, e) {
            itemCountElement.innerHTML = format('{totalItemCount:n0}個の項目', sender);
        }
    });
    var theGrid = new FlexGrid('#theGrid', {
        itemsSource: products,
        allowAddNew: true,
        allowDelete: true,
        formatItem: function (s, e) {
            if (e.panel.cellType == CellType.RowHeader) {
                //e.cell.textContent = e.row + 1;
            }
        },
        scrollPositionChanged: function () {
            var rng = theGrid.viewRange;
            products.setWindow(rng.row, rng.row2);
        }
    });
    var f = new FlexGridFilter(theGrid);
});
