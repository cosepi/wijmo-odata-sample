﻿'use strict';

require('@grapecity/wijmo.styles/wijmo.scss');

require('@grapecity/wijmo.cultures/wijmo.culture.ja');

var _grapecityWijmoOdata = require('@grapecity/wijmo.odata');

var _grapecityWijmoGrid = require('@grapecity/wijmo.grid');

var _grapecityWijmoGridFilter = require('@grapecity/wijmo.grid.filter');

$(function () {
    var url = 'https://localhost:44384/odata/';
    var products = new _grapecityWijmoOdata.ODataCollectionView(url, 'Products', {
        sortOnServer: true,
        filterOnServer: true
    });
    var itemCountElement = document.getElementById('itemCount');
    var theGrid = new _grapecityWijmoGrid.FlexGrid('#theGrid', {
        itemsSource: products
    });
    var f = new _grapecityWijmoGridFilter.FlexGridFilter(theGrid);
});

